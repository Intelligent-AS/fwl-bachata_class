ExploreBachata.exe instructions

link to .exe file https://gitlab.com/Intelligent-AS/fwl-bachata_class/-/blob/main/ExploreBachata.exe

1. Press the button "Browse" and choose a file of location system - Bachata.hdf5.
2. Press "Submit" to process the file and get information or "Cancel" to exit.
3. Choose parameters from the top window and fill the parameters: 
"Sensor name" - name of sensor or several sensors with the gaps: 100 101, 
"Channel name" - name of channel or several channels with the gaps: Z X Y,
"Component name" - name of component or several components with the gaps: zz yy,
"Point X", "Point Y", "Point depth" - local coordinates from list on the top window.
4. If you need all alternatives for any parameter mark "Show all" in the box close to the parameter.
5. If you need to see a legend of the plot - mark "Show legend" box.
5. Press the button "Show synthetic signal" to see a plot.
